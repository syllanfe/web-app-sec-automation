import Homepage from '../page-objects/myprofile-page';

class OnlineBankingPage{

    constructor() {
        try{
            this.isPageDisplayed();
        } catch (err){
            throw new Error ('The '+this.pageName+ ' is not displayed. ' + err);
        }
    }
    get pageName() { return 'Online Banking Page'; }
    get activeOnlineBankingTab() { return $("//strong[contains(text(),'Online Banking')]"); }
    get signinButton() { return $("//button[@id='signin_button']"); }

    isPageDisplayed(){
        this.activeOnlineBankingTab.waitForDisplayed(10000);
    }
    clickSigninButton(){
        this.signinButton.click();

        return new ProfilePage();
    }
}

export default OnlineBankingPage;