import OnlineBankingPage from '../page-objects/online-bank-page';
class Homepage{

    constructor() {
        try{
            this.isPageDisplayed();
        } catch (err){
            throw new Error ('The '+this.pageName+ ' is not displayed. ' + err);
        }
    }
    get pageName() { return 'Home Page'; }
    get activeHomeTab() { return $("//strong[contains(text(),'Home')]"); }
    get moreServiceButton() { return $("//a[@id='online-banking']"); }

    isPageDisplayed(){
        this.activeHomeTab.waitForDisplayed(10000);
    }

    clickMoreServices(){
        this.moreServiceButton.click();

        return new OnlineBankingPage();
    }
}

export default Homepage;