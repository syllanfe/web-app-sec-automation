class ProfilePage{

    constructor() {
        try{
            this.isPageDisplayed();
        } catch (err){
            throw new Error ('The '+this.pageName+ ' is not displayed. ' + err);
        }
    }
    get pageName() { return 'My Profile Page'; }
    get loginToZeroBank() { return $("//h3[contains(text(),'Log in to ZeroBank')]"); }

    isPageDisplayed(){
        this.loginToZeroBank.waitForDisplayed(5000);
    }


    // MyPage.prototype.login = function (username, password) {
    //     this._ui.username.setValue(username);
    //     this._ui.password.setValue(password);
    //     return this._ui.loginButton.click();
//};


//return new OnlineBankingPage();

}

export default ProfilePage;