module.exports = function () {
    var myStepDefinitionsWrapper = function () {

        this.When(/^User accesses the zerowebappsecurity site$/, function () {
            browser.url('http://zero.webappsecurity.com/');
        });

        this.Then(/^The Home Page is displayed$/, function () {

        });

        this.When(/^User enters username: "([^"]*)"$/, function (arg1, callback) {
        });

        this.When(/^password: "([^"]*)"$/, function (arg1, callback) {
        });

        this.When(/^Click on the login button$/, function () {

        });

        this.Then(/^Account Summary displayed$/, function () {
        });

        this.When(/^User clicks on Transfer Funds tab$/, function () {
        });

        this.Then(/^Transfer Funds displayed$/, function () {
        });

        this.When(/^The User selects From Account drop down box and choose Savings\(Avail\. balance = \$ 1000\)$/, function () {
        });

        this.When(/^User select To Account drop down box and choose Credit Card\(Avail\. balance = \$ \-265\)$/, function () {
        });

        this.When(/^Enter amount$/, function () {
        });

        this.When(/^Enter description$/, function () {
        });

        this.When(/^Clicks Continue button$/, function () {
        });

        this.Then(/^The Transfer Money & Make Payments \- Verify Page displayed$/, function () {
        });

        this.When(/^User clicks Submit button$/, function () {
        });

        this.Then(/^Transfer Money & Make Payments \- Confirm Page displayed$/, function () {
        });
    };
}
module.exports = myStepDefinitionsWrapper;