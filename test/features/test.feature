Feature: ZeroWebAppSecurity - Transfer and Make funds

  Scenario: The user transfer money
    When User accesses the zerowebappsecurity site
    Then The Home Page is displayed
    When User enters username: "username"
    And password: "password"
    And Click on the login button
    Then Account Summary displayed
    When User clicks on Transfer Funds tab
    Then Transfer Funds displayed
    When The User selects From Account drop down box and choose Savings(Avail. balance = $ 1000)
    And User select To Account drop down box and choose Credit Card(Avail. balance = $ -265)
    And Enter amount
    And Enter description
    And Clicks Continue button
    Then The Transfer Money & Make Payments - Verify Page displayed
    When User clicks Submit button
    Then Transfer Money & Make Payments - Confirm Page displayed